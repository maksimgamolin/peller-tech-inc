#!/bin/sh
set -e
cmd="$@"

PGPASSWORD=$DB_PASSWORD

until pg_isready -h $DB_HOST; do
  echo "Waiting for database start"
  sleep 1
done

exec $cmd
ls
python3 main.py