from http.server import BaseHTTPRequestHandler, HTTPServer
import settings

from core.request import RequestFactory


class BaseSiteHandler(BaseHTTPRequestHandler):

    def _set_headers(self, code=200):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        request = RequestFactory(self.path, self.command, self.headers, self.client_address).build()

        self._set_headers()
        self.wfile.write(b"Hello World!")


def main():
    print(f'Startup server in {settings.SERVER_HOST}:{settings.SERVER_PORT} port')
    server_address = (settings.SERVER_HOST, settings.SERVER_PORT)
    httpd = HTTPServer(server_address, BaseSiteHandler)
    httpd.serve_forever()


if __name__ == '__main__':
    main()