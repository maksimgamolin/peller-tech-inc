from collections import namedtuple

URL = namedtuple('URL', ['path', 'handler', 'name'])


class URLResolver:

    def __init__(self, patterns):
        self.patterns = patterns

    def resolve(self, request):
        pass

    def find_by_name(self, name):
        pass