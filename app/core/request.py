import urllib.parse as urllib


class Request:

    GET = {}

    def __init__(self, method, path, client_address):
        self.method = method
        self.path = path
        self.client_address = client_address


class RequestFactory:

    def __init__(self, path, method, headers, client_address):
        url, query = self.parse_url(path)
        self.r = Request(method, url.path, client_address)
        self.r.GET = query

    def parse_url(self, path):
        data = urllib.urlparse(path)
        query_data = urllib.parse_qs(data.query)
        return data, query_data

    def build(self):
        return self.r